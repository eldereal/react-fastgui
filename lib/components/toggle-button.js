"use strict";
/*jshint scripturl:true*/

import React, { createComponent, Button, Group as _Group } from 'react-fastgui';
import blacklist from 'blacklist';
import classnames from 'classnames';

function* ToggleButton(oprops, children){

    // console.info("ToggleButton", oprops, children);
    const group = this.components["ToggleButton.Group"];
    let checked;

    if(group){
        checked = oprops.value === group.value;
    }else{
        checked = this.persist("checked");
        if(oprops && oprops.checked !== undefined){
            checked = Boolean(oprops.checked);
        }

        const eventChecked = this.transient("checked");
        if(eventChecked !== undefined){
            checked = eventChecked;
        }
        if(this.persist("checked") !== checked){
            this.persist("checked", checked);
        }
    }

    const props = blacklist(oprops, "style", "onStyle", "offStyle", "checked", "value", "onClick");
    let style = oprops && oprops.style || [];
    if(!Array.isArray(style)){
        style = [style];
    }
    let overrideStyle = oprops && (checked ? oprops.onStyle : oprops.offStyle) || [];
    if(!Array.isArray(overrideStyle)){
        overrideStyle = [overrideStyle];
    }
    style = style.concat(overrideStyle);

    let clickHandler;
    if(group){
        clickHandler = group.registerToggleButton(this.ref, oprops.value);
    }else{
        clickHandler = ()=>{
            const value = !this.persist("checked");
            this.transient("checked", value);
            this.reload();
        };
    }

    yield <Button
        { ...props }
        onClick={()=>{
            clickHandler();
            if (oprops.onClick) {
                oprops.onClick();
            }
        }}
        style={ style } >
        { children }
    </Button>;

    return checked;
}

ToggleButton.componentName = "ToggleButton";

const ToggleButtonSetChecked = Symbol("ToggleButtonSetChecked");

ToggleButton.contextRef = function(obj, props, children){

    // console.info("ToggleButton.ref", this);
    const group = this.components["ToggleButton.Group"];

    if (group) {
        const config = {
            get: () => {
                return group.value === props.value;
            },
            set: (value) => {
                group.value = props.value;
            }
        };
        Object.defineProperty(obj, "checked", config);
        Object.defineProperty(obj, "value", config);
    }else{
        const config = {
            get: () => {
                const transient = this.transient("checked");
                return transient === undefined ? this.persist("checked") : transient;
            },
            set: (value) => {
                this.transient("checked", Boolean(value));
                this.reload();
            }
        };
        Object.defineProperty(obj, "checked", config);
        Object.defineProperty(obj, "value", config);
    }
};

const Nothing = Symbol("Nothing");

function* Group(oprops, children){

    let value = this.persist("value");
    if(oprops && "value" in oprops && value !== oprops.value){
        value = oprops.value;
        this.reload();
    }

    const eventValue = this.transient("value");
    if(eventValue !== undefined){
        value = eventValue;
    }
    if(this.persist("value") !== value){
        this.persist("value", value);
    }

    const props = blacklist(oprops, "value", "required");
    yield <_Group { ...props }>
        { children }
    </_Group>;
    return value;
}

Group.componentName = "ToggleButton.Group";

Group.contextRef = function(obj){

    // obj.buttons = [];
    // console.info("ToggleButton.Group.ref", this);
    obj.registerToggleButton = (ref, value) => {
        // obj.buttons.push([ref, value]);
        return () => {
            if(obj.value !== value){
                this.transient("value", value);
                this.reload();
            }
        };
    };

    Object.defineProperty(obj, "value", {
        get: () => {
            const transient = this.transient("value");
            return transient === undefined ? this.persist("value") : transient;
        },
        set: (value) => {
            this.transient("value", value);
            this.reload();
        }
    });
};

const component = createComponent(ToggleButton);
component.Group = createComponent(Group);

/*
 * Use inside a ToggleButton
 * Only shows its content when toggle button is on
 */
function *On(props, children){
    if(this.components.ToggleButton && this.components.ToggleButton.value){
        yield* children;
    }
}
On.dontExpandChildrenGenerators = true;

function *Off(props, children){
    if(this.components.ToggleButton && !this.components.ToggleButton.value){
        yield* children;
    }
}
Off.dontExpandChildrenGenerators = true;

component.On = createComponent(On);
component.Off = createComponent(Off);

export default component;

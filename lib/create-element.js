"use strict";

import symbols from "./symbols";
import makeId from './util/makeId';
import hash from 'string-hash';
import _react from 'react';
import isGenerator from './util/isGenerator';

export default function createElement(elem, props, ...children){

    if(elem[symbols.ComponentType] || elem[symbols.PseudoComponent]){
        const r = Object.create(elem, {
            props: { value: props },
            children: { value: children}
        });
        // window.a = r;
        // console.info("elem", elem, "is fastgui component:", r);
        return r;
    }else if(isGenerator(elem)){
        // console.info("elem", elem, "is generator.");
        return {
            [symbols.ComponentType]: elem,
            props: props,
            children: children
        };
    }else{
        // console.info("elem", elem, "is react component");
        if(props && makeId.hasId(props)){
            const nprops = {};
            for(const name in props){
                if(props.hasOwnProperty(name)){
                    nprops[name] = props[name];
                }
            }
            nprops.key = hash(makeId(props) + String(props.key || "")).toString(36);
            // console.info("create react element", elem, props, nprops, children);
            return _react.createElement(elem, nprops, ...children);
        }else{
            return _react.createElement(elem, props, ...children);
        }
    }
}

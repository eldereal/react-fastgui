"use strict";

export const Id = Symbol("Id");
export const ComponentType = Symbol("ComponentType");
export const Component = Symbol("Component");
export const TransientResult = Symbol("TransientResult");
export const PrivateProperties = Symbol("PrivateProperties");
export const PseudoComponent = Symbol("PseudoComponent");

export default {
    Id: Id,
    ComponentType: ComponentType,
    Component: Component,
    TransientResult: TransientResult,
    PrivateProperties: PrivateProperties,
    PseudoComponent: PseudoComponent
};

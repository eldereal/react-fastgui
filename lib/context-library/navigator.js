"use strict";

export default function(ctx){


    return {
        push(item){
            return ctx.backend.navigator.push(item);
        },
        pop(result){
            return ctx.backend.navigator.pop(result);
        },
        show(item){
            return ctx.backend.navigator.show(item);
        },
        replace(item){
            return ctx.backend.navigator.replace(item);
        }
    };
}

"use strict";

import React from 'react';
import {
    createSymbol,
    Id as FastGuiId,
    PrivateProperties,
    TransientResult,
    ComponentType,
    Component,
} from './symbols';
import collectIterator from './util/collectIterator';
import isGenerator from './util/isGenerator';
import isPrimitive from 'is-primitive';
import navigatorFactory from './context-library/navigator';
import makeId from './util/makeId';
import extend from 'babel-runtime/helpers/extends';
import StyleSheet from './library/stylesheet';

const _private = {
    run: Symbol("run")
};

const fakeHashForInlineComponents = {
    __hash: "__inline"
};

export default class Context {

    constructor(component, id, parent) {
        this.id = id;
        this.component = component;
        this.refs = Object.create(parent && parent.refs || null);
        this.components = Object.create(parent && parent.components || null);
    }

    // get or set transient data. transient data is cleared after each ongui calls and is not part of state.
    transient(key, value) {
        if (arguments.length === 1) {
            const store = this.component.transient(this.id);
            return store && store[key];
        } else if (arguments.length > 1) {
            const store = this.component.transient(this.id) || {};
            store[key] = value;
            this.component.transient(this.id, store);
            // console.info("get transient value ",this.id + "." + key, "=", value);
            return value;
        }
    }

    // get or set persist data. persist data is part of component state.
    persist(key, value) {
        if (arguments.length === 1) {
            const store = this.component.persist(this.id);
            return store && store[key];
        } else if (arguments.length > 1) {
            const store = this.component.persist(this.id) || {};
            store[key] = value;
            this.component.persist(this.id, store);
            return value;
        }
    }

    reload() {
        this.component.reload();
    }

    *[_private.run](iterable, props, children, options) {
        let iterator;
        if (isGenerator(iterable)) {
            let processedChildren;
            if (children && !iterable.dontExpandChildrenGenerators) {
                // const sub = new Context(this.component, makeId(props, this.id), this);
                processedChildren = Array.from(this[_private.run](children));
            } else {
                processedChildren = children;
            }
            iterator = iterable.call(this, props, processedChildren, options);
            if (!iterator.next) {
                if (iterator[Symbol.iterator]) { //return iterable instead of iterator
                    iterator = iterator[Symbol.iterator];
                } else {
                    throw new Error("FastGui function component must return an iterable object or iterator");
                }
            }
        } else if (iterable[Symbol.iterator]) {
            iterator = iterable[Symbol.iterator]();
        } else if (Array.isArray(iterable)) {
            //in modern environment which supports Symbol.iterator this branch will shortcuted by previous branch
            iterator = (function * () {
                for (const i in iterable) {
                    yield iterable[i];
                }
            })();
        } else {
            console.error("cannot process iterable", typeof iterable, iterable);
            throw new Error("FastGui component must be an iterable object or generator function");
        }

        let nextVal;
        let aggregateText;
        while (true) {
            const result = iterator.next(nextVal);
            if (result.done) {
                if (aggregateText) {
                    yield * this.backend.rawText.apply(this, aggregateText);
                    aggregateText = null;
                }
                return result.value;
            }
            const value = result.value;
            const [ isComponent, item ] = dispatchBackendComponent.call(this, value);
            // console.info("process", result.value, isComponent, item);
            if (isPrimitive(item)) {
                if (!aggregateText) {
                    aggregateText = [item];
                } else {
                    aggregateText.push(item);
                }
                continue;
            } else if (aggregateText) {
                yield * this.backend.rawText.apply(this, aggregateText);
                aggregateText = null;
            }
            // console.info("process", item);
            if (item === null || item === undefined) { //skip null elements;

            } else if (isGenerator(item)) { //generator yields another generator (expanding inline component)
                // const sub = new Context(this.component, makeId(fakeHashForInlineComponents, this.id));
                if (isComponent) {
                    // console.info("process component", item, value.props, value.children);
                    const sub = new Context(this.component, makeId(value.props, this.id), this);
                    let componentProps;
                    const componentStyle = StyleSheet.component(value);
                    if (componentStyle) {
                        componentProps = mergeProps(value.props, {
                            style: componentStyle
                        });
                    } else {
                        componentProps = value.props;
                    }

                    if (item.contextRef) {
                        const obj = {};
                        item.contextRef.call(sub, obj, componentProps, value.children);
                        sub.ref = obj;
                        if (item.componentName) {
                            this.components[item.componentName] = obj;
                        }
                        if (value.props.contextRef) {
                            this.refs[value.props.contextRef] = obj;
                        }
                    }
                    
                    nextVal = yield * sub[_private.run](item, componentProps, value.children);
                } else {
                    nextVal = yield * this[_private.run](item, value.props, value.children);
                }
            } else if (isComponent) {
                yield Object.create(item, {
                    props: {
                        value: value.props
                    },
                    children: {
                        value: value.children
                    },
                });
            } else {
                nextVal = item[TransientResult];
                yield item;
            }
        }

        function dispatchBackendComponent(item) {
            /* jshint validthis:true */
            if (item === undefined || item === null) {
                return [false, item,];
            } else if (item[ComponentType]) {
                const comp = item[ComponentType];
                if (typeof comp === "string") {
                    const compObj = this.backend.components[comp];
                    if (!compObj) {
                        console.warn("Your backend doesn't provide component", item[ComponentType], " it will be silencely ignored.");
                        return [false, null,];
                    }
                    return [
                        true,
                        dispatchBackendComponent.call(this, compObj)[1],
                    ];
                } else {
                    return [
                        true,
                        dispatchBackendComponent.call(this, comp)[1],
                    ];
                }
            } else {
                return [false, item,];
            }
        }
    }

    get backend() {
        return this.component.props.backend;
    }

    get theme() {
        return this.component.props.theme;
    }

    get navigator() {
        if (!this._navigator) {
            this._navigator = navigatorFactory.call(this, this);
        }
        return this._navigator;
    }

    static get[PrivateProperties]() {return _private;}
}

function mergeProps(props, option) {
    if (!option) {
        return;
    }
    let nprops;
    if (option.style) {
        nprops = extend({}, props);
        if(nprops.style){
            if (Array.isArray(nprops.style)) {
                nprops.style.splice(0, 0, option.style);
            } else {
                nprops.style = [option.style, nprops.style];
            }
        } else {
            nprops.style = option.style;
        }
    }
    return nprops || props;
}
//
// function merge(v1, v2){
//     if(!v1){
//         return v2;
//     }
//     if(!v2){
//         return v1;
//     }
//     return extend({}, v1, v2);
// }

"use strict";

import symbols from "./symbols";

import ToggleButton from "./components/toggle-button";

export default Object.create(null, {
    // componentNames: ["Button", "Label", "List", "TextBox", "Group"],
    Button: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Button" }}) },
    ToggleButton: { value: ToggleButton },
    Page: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Page" }}) },
    Label: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Label" }}) },
    List: { value:  Object.create(null, {[symbols.ComponentType]: { value: "List" }}) },
    TextBox: { value:  Object.create(null, {[symbols.ComponentType]: { value: "TextBox" }}) },
    Group: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Group" }}) },
    Image: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Image" }}) },
    Select: { value:  Object.create(null, {
        [symbols.ComponentType]: { value: "Select" },
        Option: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Select.Option" }}) },
    })},
    ScrollPane: { value:  Object.create(null, {[symbols.ComponentType]: { value: "ScrollPane" }}) },
    Alert: { value:  Object.create(null, {
        [symbols.ComponentType]: { value: "Alert" },
        Option: { value:  Object.create(null, {[symbols.ComponentType]: { value: "Alert.Option" }}) }
    })},
});

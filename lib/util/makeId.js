"use strict";
import hash from 'string-hash';

function makeId(props, parentId, contextElement){
    if(!props){
        throw new Error("FastGui elements require id, __uuid, or __source property to identity elements. If your compiler is babeljs, you can use \"transform-react-jsx-source\" plugin to add __source property.");
    }else if(props.id){
        return props.id;
    }else if(props.__source){
        const fullKey = (parentId || "") + props.__source.fileName + props.__source.lineNumber + (props.key || "");
        // console.info(fullKey, hash(fullKey).toString(36));
        return String(hash(fullKey).toString(36));
    }else if(props.__uuid){
        const fullKey = (parentId || "") + props.__uuid + (props.key || "");
        // console.info(fullKey, hash(fullKey).toString(36));
        return String(hash(fullKey).toString(36));
    }else{
        throw new Error("FastGui elements require id, __uuid, or __source property to identity elements. If your compiler is babeljs, you can use \"transform-react-jsx-source\" plugin to add __source property. The props is " + JSON.stringify(props));
    }
}

makeId.hasId = function(props){
    return props.id || props.__source || props.__uuid;
};

export default makeId;

"use strict";

export default function(){
    return (new Date().getTime().toString(36).substr(-4)) + (Math.random().toString(36).substr(-4));
}

"use strict";

export default function(iterator){
    const children = [];
    while(true){
        const item = iterator.next();
        if(item.done){
            return [item.value, children];
        }else{
            children.push(item.value);
        }
    }
}

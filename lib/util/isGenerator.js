"use strict";

import regenerator from 'babel-runtime/regenerator';

export default function(target){
    return regenerator.isGeneratorFunction(target);
}

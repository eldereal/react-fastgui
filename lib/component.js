"use strict";

import { Component as ReactComponent } from 'react';
import { TransientResult } from './symbols';
import extend from 'babel-runtime/helpers/extends';
import generateUniqueId from './util/generateUniqueId';
export default ReactComponent;

"use strict";

import hash from 'object-hash';

const PersistStyleSheet = Symbol("PersistStyleSheet");
const ComponentStyleSheet = Symbol("ComponentStyleSheet");

function create(obj){
    const clone = JSON.parse(JSON.stringify(obj));
    freezeRec(clone);
    return clone;

    function freezeRec(obj){
        for(const key in obj){
            if(typeof obj[key] === "object" && obj[key] !== null){
                freezeRec(obj[key]);
            }
        }
        obj[PersistStyleSheet] = { id: hash(obj) };
        Object.freeze(obj);
    }
}

export function isPersistedStyleSheet(obj){
    return Boolean(obj[PersistStyleSheet]);
}

export function getPersistId(obj){
    return obj[PersistStyleSheet].id;
}

export function setPersistData(obj, key, value){
    obj[PersistStyleSheet][key] = value;
}

export function getPersistData(obj, key){
    return obj[PersistStyleSheet][key];
}

export function component(comp, style){
    if(arguments.length === 1){
        return comp[ComponentStyleSheet];
    }else{
        comp[ComponentStyleSheet] = create(style);
    }
}

const stylesheet = {
    create: create,
    isPersistedStyleSheet: isPersistedStyleSheet,
    getPersistId: getPersistId,
    setPersistData: setPersistData,
    getPersistData: getPersistData,
    component: component
};

Object.freeze(stylesheet);

export default stylesheet;

"use strict";


const store = {};

export default {
    get: function(dims){
        const val = store[dims];
        if(typeof val === "function"){
            return val();
        }else{
            return val;
        }
    },
    set: function(dims){
        for(const key in dims){
            store[key] = dims[key];
        }
    }
};

"use strict";

import Context from './context';
import {PrivateProperties, TransientResult} from './symbols';
import collectIterator from './util/collectIterator';
import Component from './component';
import builtInComponents from './built-in-components';
import createElement from './create-element';
import extend from 'babel-runtime/helpers/extends';

const React = { createElement: createElement };
const Page = builtInComponents.Page;

const run = Context[PrivateProperties].run;

const MaxUpdateLoop = 10;

const _element = Symbol("element");
const _updateCount = Symbol("updateCount");
const _nextReloadScheduled = Symbol("nextReloadScheduled");
const _nextReloadTransient = Symbol("nextReloadTransient");
const _unmounted = Symbol("unmounted");

// const _backend = Symbol("_backend");
const _private = {
    element: _element,
    updateCount: _updateCount,
    nextReloadScheduled: _nextReloadScheduled,
    nextReloadTransient: _nextReloadTransient,
    unmounted: _unmounted
    // backend: _backend
};

export default class GUIComponent extends Component {

    constructor(props){
        if(!props.backend || !props.ongui || !props.id){
            throw new Error("FastGui Component require 'backend', 'ongui' and 'id' props.");
        }
        super(props);
    }

    // get or set transient data. transient data is cleared after each ongui calls and is not part of state.
    // special case: If called in gui update functions, data will be keep to next reload
    transient(key, value){
        // console.info("transient", this.transientStore, arguments);
        if (arguments.length === 1) {
            return (this[_nextReloadTransient] && this[_nextReloadTransient][key] !== undefined && this[_nextReloadTransient][key]) ||
                (this.transientStore && this.transientStore[key]);
        } else if (arguments.length > 1) {
            if(this[_nextReloadTransient]){
                this[_nextReloadTransient][key] = value;
            }else if(this[_nextReloadTransient] === null){
                this[_nextReloadTransient] = {
                    [key]: value
                };
            }else if(!this.transientStore){
                this.transientStore = {
                    [key]: value
                };
            }else{
                this.transientStore[key] = value;
            }
            // this.reload();
            return value;
        }
    }

    // get or set persist data. persist data is part of component state.
    persist(key, value){
        if (arguments.length === 1) {
            // console.info("get persist state", key, this.state && this.state[key]);
            return (this.stateUpdates && this.stateUpdates[key]) || (this.state && this.state[key]);
        } else if (arguments.length > 1) {
            if(!this.stateUpdates){
                this.stateUpdates = Object.create(null);
            }
            this.stateUpdates[key] = value;
            // this.reload();
            return value;
        }
    }

    componentWillMount(){
        this.reload();
    }

    componentDidMount(){
        if(this.props.onReady){
            this.props.onReady();
        }
    }

    componentWillUnmount(){
        // console.info("unmount", this);
        this[_unmounted] = true;
    }

    componentWillUpdate(){
        if(this[_element] === undefined){
            this[_element] = null;
            this.reload();
        }
    }

    reload(){
        // console.info("reload", this.props.id, this.unmounted);
        if(this[_unmounted]){
            return;
        }
        const count = this[_updateCount] || 0;
        if(count > MaxUpdateLoop){
            console.error("infinite reload loop, don't set persist/transient data unconditionally in GUI functions. Further updates will be ignored.");
            return;
        }
        if(!this[_nextReloadScheduled]){
            this[_nextReloadScheduled] = setTimeout(() => this.updateGuiImpl());
        }
    }

    updateGuiImpl(){
        // console.info("updateGuiImpl", this.props.id, this.unmounted);
        if(this[_unmounted]){
            return;
        }
        // console.info("ongui", this);
        const ctx = new Context(this, this.props.id, this.props.parentComponent && this.props.parentComponent.context);
        // this.context = ctx;
        this[_nextReloadScheduled] = false;
        this[_updateCount] =  (this[_updateCount] || 0) + 1;
        try{
            const _this = this;
            this[_nextReloadTransient] = null;
            const [res, children] = collectIterator(ctx[run](
                function*(){
                    yield <Page { ..._this.props }>
                        { _this.props.ongui }
                    </Page>;
                    // createElement(Group, extend({__source:{
                    //     fileName: "__root",
                    //     lineNumber: 0
                    // }}, _this.props), _this.props.ongui);
                }));
            // console.info("updateGuiImpl", res, children);
            this[TransientResult] = res;
            this[_element] = children[0] || null;
        }finally{
            // console.info("clear transient store");
            this.transientStore = this[_nextReloadTransient] || undefined;
            this[_nextReloadTransient] = undefined;
            if(this[_nextReloadScheduled]){//there are some update during this ongui call

            }else if(!this[_unmounted]){//there is no further updates, now we can update react component
                this[_updateCount] = 0;
                if(this.stateUpdates){//merge persist data into component state
                    // console.info("state update", this.stateUpdates);
                    this.setState(this.stateUpdates);
                    this.stateUpdates = undefined;
                }else{
                    // console.info("forceUpdate", this.props.id);
                    this.forceUpdate();
                }
            }
        }
    }

    render() {
        const elem = this[_element];
        // console.info("render", elem);

        if(!elem){
            return null;
        }
        return elem;
    }

    // get [_backend](){
    //     return (this.props && this.props.config && this.props.config.backend) || config.backend || defaultConfig.backend;
    // }

    static get [PrivateProperties](){
        return _private;
    }
}

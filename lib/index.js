"use strict";
/*jshint loopfunc:true*/

import symbols from "./symbols";


import _GUIComponent from './gui-component';
import _Component from './component';
import builtInComponents from './built-in-components';
import createElement from './create-element';
import _react from 'react';
import _makeId from './util/makeId';
import dimensions from './library/dimensions';
import stylesheet from './library/stylesheet';

export const defaultConfig = Object.create(null);

export const config = Object.create(defaultConfig);

export const StyleSheet = stylesheet;

const fastgui = {
    components: builtInComponents,
    Component: _Component,
    GUIComponent: _GUIComponent,
    createElement: createElement,
    createComponent: createComponent,
    createPseudoComponent: createPseudoComponent,
    StyleSheet: StyleSheet,
    Dimensions: dimensions
};

Object.freeze(fastgui);

export default fastgui;
export const Button = fastgui.components.Button;
export const ToggleButton = fastgui.components.ToggleButton;
export const Page = fastgui.components.Page;
export const Label = fastgui.components.Label;
export const List = fastgui.components.List;
export const Image = fastgui.components.Image;
export const TextBox = fastgui.components.TextBox;
export const Select = fastgui.components.Select;
export const ScrollPane = fastgui.components.ScrollPane;
export const Alert = fastgui.components.Alert;
export const Group = fastgui.components.Group;
export const GUIComponent = fastgui.GUIComponent;
export const Component = fastgui.Component;
export const Dimensions = fastgui.Dimensions;
export const makeId = _makeId;
export const React = fastgui;

export function bootstrap(be, ongui, options){
    if(!be){
        throw new Error("You should pass a valid backend to bootstrap, for example 'https://github.com/eldereal/react-fastgui-backend-html'");
    }
    console.log("bootstrapping react-fastgui");
    be.bootstrap(ongui, options || {});
}

export function createComponent(generator, props){
    const config = {[symbols.ComponentType]: { value: generator }};
    for(const key in props){
        config[key] = { value: props.key };
    }
    return Object.create(Object.prototype, config);
}

export function createPseudoComponent(generator, props){
    const config = {[symbols.PseudoComponent]: { value: generator }};
    for(const key in props){
        config[key] = { value: props.key };
    }
    return Object.create(Object.prototype, config);
}
